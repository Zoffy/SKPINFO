﻿using System;
using System.Globalization;
using System.IO;
using System.Runtime.InteropServices;
using System.Text;

namespace SKPINFO
{
    public class SkpApi
    {
        //税控盘接口//
        [DllImport("skpc.dll", EntryPoint = "OperateDisk", CharSet = CharSet.Ansi)]                 //载入DLL;
        public static extern IntPtr OperateDisk(String 传入的XML, byte[] 传出的XML);                //定义;
        public static string sOutputInfo;                                                           //返回报文;

        /// <summary>
        /// 获取税控盘连接状态
        /// </summary>
        /// <returns>成功返回true 失败返回false</returns>
        public static string Skpinfo(string Password)
        {
            //声明
            string skpinfo = string.Empty;

            //读取税控盘;
            string sInputInfo = @"<?xml version=""1.0"" encoding=""gbk""?><business comment=""税控盘信息查询"" id=""SKPXXCX""><body yylxdm=""1""><input><skpkl>" + Password + "</skpkl></input></body></business>";//传入的XML;
            byte[] buffer = new byte[1048576];                                                      //设置字节1M;
            OperateDisk(sInputInfo, buffer);                                                        //执行连接;
            sOutputInfo = Encoding.Default.GetString(buffer, 0, Array.IndexOf(buffer, (byte)0));    //编码DLL返回的XML;

            var doc = new System.Xml.XmlDocument();                                                 //实例化XmlDocument;
            doc.LoadXml(sOutputInfo);                                                               //加载Xml;
            string returncode = doc.SelectSingleNode("business/body/output/returncode").InnerText;  //取税控盘连接状态;
            if (returncode == "0")
            {
                string returnmsg = doc.SelectSingleNode("business/body/output/returnmsg").InnerText;
                string skpbh = doc.SelectSingleNode("business/body/output/skpbh").InnerText;
                string nsrsbh = doc.SelectSingleNode("business/body/output/nsrsbh").InnerText;
                string nsrmc = doc.SelectSingleNode("business/body/output/nsrmc").InnerText;
                string swjgdm = doc.SelectSingleNode("business/body/output/swjgdm").InnerText;
                string swjgmc = doc.SelectSingleNode("business/body/output/swjgmc").InnerText;
                string fplxdm = doc.SelectSingleNode("business/body/output/fplxdm").InnerText;
                string dqsz = doc.SelectSingleNode("business/body/output/dqsz").InnerText;
                string qysj = doc.SelectSingleNode("business/body/output/qysj").InnerText;
                string bbh = doc.SelectSingleNode("business/body/output/bbh").InnerText;
                string kpjh = doc.SelectSingleNode("business/body/output/kpjh").InnerText;
                string qylx = doc.SelectSingleNode("business/body/output/qylx").InnerText;
                string blxx = doc.SelectSingleNode("business/body/output/blxx").InnerText;
                string qtkzxx = doc.SelectSingleNode("business/body/output/qtkzxx").InnerText;

                skpinfo = string.Format("{0}\r\n{1}\r\n{2}\r\n{3}\r\n{4}\r\n{5}\r\n{6}\r\n{7}\r\n{8}\r\n{9}\r\n{10}\r\n{11}\r\n{12}\r\n{13}\r\n",
                          returnmsg, skpbh, nsrsbh, nsrmc, swjgdm, swjgmc, Getpz(fplxdm), Todate(dqsz), Todate(qysj), bbh, kpjh, Getqylx(qylx), Getblxx(blxx), qtkzxx);
            }
            else
            {
                return doc.SelectSingleNode("business/body/output/returnmsg").InnerText;
            }
            return skpinfo;                                                             //返回税控盘信息
        }

        private static string Getpz (string pzcode)
        {
            string pz = string.Empty;
            if (pzcode.Contains("007"))
            {
                pz += "普票 | ";
            }
            if (pzcode.Contains("004"))
            {
                pz += "专票 | ";
            }
            if (pzcode.Contains("026"))
            {
                pz += "电票 | ";
            }
            if (pzcode.Contains("005"))
            {
                pz += "机动车票 | ";
            }
            if (pzcode.Contains("006"))
            {
                pz += "二手车票 | ";
            }
            if (pzcode.Contains("025"))
            {
                pz += "卷票 | ";
            }

            pz = pz.Substring(0, pz.Length - 3);
            return pz;
        }

        private static string Getqylx(string qylxcode)
        {
            string qylx = string.Empty;
            if(qylxcode == "00" )
            {
                qylx = "非特殊企业";
            }

            if (qylxcode == "01")
            {
                qylx = "特殊企业";
            }
            return qylx;
        }

        private static string Getblxx(string blxxcode)
        {
            string blxx = string.Empty;
            string[] code = Getstr(blxxcode, 2);

            switch(code[0])
            {
                case ("00"):
                    blxx += "非农产品企业\r\n";
                    break;
                case ("01"):
                    blxx += "销售农产品企业\r\n";
                    break;
                case ("02"):
                    blxx += "收购农产品企业\r\n";
                    break;
                case ("03"):
                    blxx += "销售和收购农产品企业\r\n";
                    break;
                default:
                    blxx += "未知类型:" + code[0] + "\r\n";
                    break;
            }

            switch (code[1])
            {
                case ("00"):
                    blxx += "非铁路相关纳税人\r\n";
                    break;
                case ("01"):
                    blxx += "铁道部集中缴纳纳税人\r\n";
                    break;
                case ("02"):
                    blxx += "跨省合资铁路企业纳税人\r\n";
                    break;
                default:
                    blxx += "未知类型:" + code[1] + "\r\n";
                    break;
            }

            switch (code[2])
            {
                case ("01"):
                    blxx += "一般纳税人\r\n";
                    break;
                case ("08"):
                    blxx += "小规模纳税人\r\n";
                    break;
                default:
                    blxx += "未知类型:"+code[2]+"\r\n";
                    break;
            }

            blxx = blxx.TrimEnd((char[])"\r\n".ToCharArray());
            return blxx;
        }



        private static string[] Getstr(string strs, int len)
        {
            double i = strs.Length;
            string[] myarray = new string[int.Parse(Math.Ceiling(i / len).ToString())];
            for (int j = 0; j < myarray.Length; j++)
            {
                len = len <= strs.Length ? len : strs.Length;
                myarray[j] = strs.Substring(0, len);
                strs = strs.Substring(len, strs.Length - len);
            }
            return myarray;
        }


        private static string Todate (string date)
        {
            string datetime = DateTime.ParseExact(date, "yyyyMMddHHmmss",CultureInfo.InvariantCulture).ToString();
            return datetime;
        }


        public static void DelectDir(string srcPath)
        {
            try
            {
                DirectoryInfo dir = new DirectoryInfo(srcPath);
                FileSystemInfo[] fileinfo = dir.GetFileSystemInfos();
                foreach (FileSystemInfo i in fileinfo)
                {
                    if (i is DirectoryInfo)
                    {
                        DirectoryInfo subdir = new DirectoryInfo(i.FullName);
                        subdir.Delete(true);
                    }
                    else
                    {
                        File.Delete(i.FullName);
                    }
                }
            }
            catch
            {
                throw;
            }
        }
    }
}
